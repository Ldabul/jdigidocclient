package com.nortal.sk.client.processor;

public interface Processor {
  Processor step(Step step);

  Processor step(WSStep step);

  StateHolder process() throws Exception;
}

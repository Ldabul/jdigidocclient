package com.nortal.sk.client.processor;

import com.nortal.sk.ws.DigiDocServiceClient;

public interface WSStep extends Step {
  void setClient(DigiDocServiceClient client);
}

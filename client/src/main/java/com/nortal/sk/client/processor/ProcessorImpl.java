package com.nortal.sk.client.processor;

import java.util.LinkedHashMap;
import java.util.Map;

import com.nortal.sk.client.constant.StepCodeEnum;
import com.nortal.sk.ws.DigiDocServiceClient;

/**
 * @author Lauri Lättemäe <lauri.lattemae@nortal.com>
 */
public class ProcessorImpl implements Processor {
  private DigiDocServiceClient client;
  private StateHolder state = new StateHolder();
  private Map<StepCodeEnum, Step> steps = new LinkedHashMap<>();

  @Override
  public StateHolder process() throws Exception {
    if (state.isComplete()) {
      return state;
    }

    //    if (!steps.containsKey(StepCodeEnum.CLOSE_SESSION)) {
    //      step(new CloseSessionStep());
    //    }

    for (Step step : steps.values()) {
      if (step.isComplete()) {
        continue;
      }
      step.execute();

      if (!step.isValid() || !step.isComplete() || step.isReturning()) {
        return state;
      }
    }
    state.complete();
    return state;
  }

  @Override
  public ProcessorImpl step(Step step) {
    if (steps.containsKey(step.getCode())) {
      throw new RuntimeException("Processor already have step " + step.getCode() + " included");
    }
    step.setState(state);
    steps.put(step.getCode(), step);
    return this;
  }

  @Override
  public ProcessorImpl step(WSStep step) {
    if (steps.containsKey(step.getCode())) {
      throw new RuntimeException("Processor already have step " + step.getCode() + " included");
    }
    step.setState(state);
    step.setClient(client);
    steps.put(step.getCode(), step);
    return this;
  }

  public void setClient(DigiDocServiceClient client) {
    this.client = client;
  }

  public static ProcessorImpl of(StateHolder state) {
    ProcessorImpl processor = new ProcessorImpl();
    if (state != null) {
      processor.state = state;
    }
    return processor;
  }
}

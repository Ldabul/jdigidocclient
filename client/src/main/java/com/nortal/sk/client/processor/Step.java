package com.nortal.sk.client.processor;

import com.nortal.sk.client.constant.StepCodeEnum;

/**
 * @author Lauri Lättemäe <lauri.lattemae@nortal.com>
 */
public interface Step {
  void setState(StateHolder state);

  StepCodeEnum getCode();

  void execute() throws Exception;

  boolean isReturning();

  boolean isValid();

  boolean isComplete();
}

package com.nortal.sk.client.processor;

import com.nortal.sk.client.constant.StepCodeEnum;
import com.nortal.sk.ws.constant.MobileSignStatusEnum;
import com.nortal.sk.ws.constant.StatusEnum;
import com.nortal.sk.ws.model.GeneralRsp;
import com.nortal.sk.ws.model.StatusInfoReq;
import com.nortal.sk.ws.model.StatusInfoReqImpl;
import com.nortal.sk.ws.model.StatusInfoRsp;

/**
 * @author Lauri Lättemäe <lauri.lattemae@nortal.com>
 */
public class StatusInfoStep extends WSStepImpl<StatusInfoReq> {
  {
    setCode(StepCodeEnum.STATUS_INFO);
    setValidStatus(StatusEnum.OK);
    setType(StatusInfoReqImpl.class);
  }

  @Override
  public boolean isComplete() {
    if (!super.isComplete()) {
      return false;
    }
    StatusInfoRsp rsp = getResponse();
    return MobileSignStatusEnum.SIGNATURE.equals(MobileSignStatusEnum.valueOf(rsp.getStatusCode()));
  }

  @Override
  protected GeneralRsp innerExecute() throws Exception {
    StatusInfoReq req = prepareRequest();
    setReturning(!req.isWaitSignature());
    return getClient().getStatusInfo(req);
  }

  public static StatusInfoStep of(StatusInfoReq req) {
    StatusInfoStep step = new StatusInfoStep();
    step.setInput(req);
    return step;
  }
}

package com.nortal.sk.client;

import com.nortal.sk.client.processor.Processor;
import com.nortal.sk.client.processor.ProcessorImpl;
import com.nortal.sk.client.processor.StateHolder;
import com.nortal.sk.ws.DigiDocServiceClient;

/**
 * @author Lauri Lättemäe <lauri.lattemae@nortal.com>
 */
public class AbstractDigiDocClient implements DigiDocClient {
  private DigiDocServiceClient digiDocWSClient;

  public void setDigiDocWSClient(DigiDocServiceClient digiDocWSClient) {
    this.digiDocWSClient = digiDocWSClient;
  }

  @Override
  public Processor createProcessor() {
    return createProcessor(null);
  }

  @Override
  public Processor createProcessor(StateHolder state) {
    ProcessorImpl processor = ProcessorImpl.of(state);
    processor.setClient(digiDocWSClient);
    return processor;
  }
}

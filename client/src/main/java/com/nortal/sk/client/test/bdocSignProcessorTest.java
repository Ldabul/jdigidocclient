package com.nortal.sk.client.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.nortal.sk.client.DigiDocClient;
import com.nortal.sk.client.DigiDocClientUtils;
import com.nortal.sk.client.processor.MobileSignStep;
import com.nortal.sk.client.processor.Processor;
import com.nortal.sk.client.processor.SignedDocStep;
import com.nortal.sk.client.processor.StartSessionStep;
import com.nortal.sk.client.processor.StateHolder;
import com.nortal.sk.client.processor.StatusInfoStep;
import com.nortal.sk.client.processor.StepDataCallback;
import com.nortal.sk.ws.model.ChallengeRsp;
import com.nortal.sk.ws.model.FaultRsp;
import com.nortal.sk.ws.model.MobileSignReqImpl;

import ee.sk.digidoc.DigiDocException;
import ee.sk.digidoc.SignedDoc;

public class bdocSignProcessorTest {

  private static String ID_CODE = "38301080372";
  private static String PHONE_NO = "3725235902";

  private static SignedDoc getDoc() throws FileNotFoundException, DigiDocException {
    return DigiDocClientUtils.readSignedDoc(new File("C:/projects/jDigiDocClient/client/test3.bdoc"));
    //    return DigiDocClientUtils.readSignedDoc(new File("C:/projects/jDigiDocClient/client/test.ddoc"));
  }

  private static StepDataCallback<String> getDocCallback() {
    return new StepDataCallback<String>() {
      @Override
      public String getData() {
        //        return StringUtils.EMPTY;
        try {
          return DigiDocClientUtils.toWSFormat(getDoc());
        }
        catch (DigiDocException | IOException | JAXBException e) {
          throw new RuntimeException("", e);
        }
      }
    };
  }

  public static void main(String[] args) {
    try {
      System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
      ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");

      StateHolder state = StateHolder.of(getDoc());
      DigiDocClient client = context.getBean(DigiDocClient.class);
      do {
        // @formatter:off
        Processor processor = client.createProcessor(state)
            .step(new StartSessionStep())
            .step(MobileSignStep.of(MobileSignReqImpl.of(ID_CODE, PHONE_NO)))
            .step(new StatusInfoStep())
            .step(new SignedDocStep());
        // @formatter:on
        state = processor.process();

        if (state.isValid()) {
          switch (state.getActiveStep()) {
            case MOBILE_SIGN:
              ChallengeRsp rsp = state.getActiveResponse();
              System.out.println("CHALLENGE: " + rsp.getChallengeID());
              break;
            case STATUS_INFO:
              Thread.currentThread().sleep(2000);
            default:
              System.out.println(state.getActiveStep());
              break;
          }
        }
      } while (!state.isComplete());

      if (state.isValid()) {
        state.getDoc().writeToStream(new FileOutputStream("C:/projects/jDigiDocClient/client/test4.bdoc"));
      }
      else {
        FaultRsp rsp = state.getActiveResponse();
        System.out.println("code=" + rsp.getCode() + ", message=" + rsp.getMessage());
      }
    }
    catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
}

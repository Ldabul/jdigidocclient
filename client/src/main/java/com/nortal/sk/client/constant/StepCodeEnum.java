package com.nortal.sk.client.constant;

import com.nortal.sk.ws.constant.GeneralConstant;

public enum StepCodeEnum implements GeneralConstant {
  PREPARING,

  CARD_SIGN_START,
  CARD_SIGN_FINALIZE,
  SIGNATURE_CONFIRMATION,

  MOBILE_AUTHENTICATE,
  MOBILE_AUTHENTICATE_STATUS,

  START_SESSION,
  MOBILE_SIGN,
  STATUS_INFO,
  SIGNED_DOC,
  CLOSE_SESSION;

  @Override
  public String getCode() {
    return name();
  }
}

package com.nortal.sk.client.processor;

import com.nortal.sk.client.constant.StepCodeEnum;
import com.nortal.sk.ws.constant.MobileAuthStatusEnum;
import com.nortal.sk.ws.model.GeneralRsp;
import com.nortal.sk.ws.model.MobileAuthenticateStatusReq;
import com.nortal.sk.ws.model.MobileAuthenticateStatusReqImpl;

/**
 * @author Lauri Lättemäe <lauri.lattemae@nortal.com>
 */
public class MobileAuthenticateStatusStep extends WSStepImpl<MobileAuthenticateStatusReq> {
  {
    setCode(StepCodeEnum.MOBILE_AUTHENTICATE_STATUS);
    setValidStatus(MobileAuthStatusEnum.USER_AUTHENTICATED);
    setType(MobileAuthenticateStatusReqImpl.class);
  }

  @Override
  protected GeneralRsp innerExecute() throws Exception {
    MobileAuthenticateStatusReq req = prepareRequest();
    setReturning(!req.isWaitSignature());
    return getClient().getMobileAuthenticateStatus(req);
  }

  public static MobileAuthenticateStatusStep of(MobileAuthenticateStatusReq req) {
    MobileAuthenticateStatusStep step = new MobileAuthenticateStatusStep();
    step.setInput(req);
    return step;
  }
}

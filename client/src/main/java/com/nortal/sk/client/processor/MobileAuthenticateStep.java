package com.nortal.sk.client.processor;

import com.nortal.sk.client.constant.StepCodeEnum;
import com.nortal.sk.ws.model.GeneralRsp;
import com.nortal.sk.ws.model.MobileAuthenticateReq;
import com.nortal.sk.ws.model.MobileAuthenticateReqImpl;

public class MobileAuthenticateStep extends WSStepImpl<MobileAuthenticateReq> {
  {
    setCode(StepCodeEnum.MOBILE_AUTHENTICATE);
    setType(MobileAuthenticateReqImpl.class);
    setReturning(true);
  }

  @Override
  protected GeneralRsp innerExecute() throws Exception {
    return getClient().mobileAuthenticate((MobileAuthenticateReq) prepareRequest());
  }

  public static MobileAuthenticateStep of(MobileAuthenticateReq req) {
    MobileAuthenticateStep step = new MobileAuthenticateStep();
    step.setInput(req);
    return step;
  }
}

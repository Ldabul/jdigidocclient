package com.nortal.sk.client.processor;

import org.apache.commons.lang3.StringUtils;

import com.nortal.sk.client.DigiDocClientUtils;
import com.nortal.sk.client.constant.StepCodeEnum;
import com.nortal.sk.ws.model.GeneralRsp;
import com.nortal.sk.ws.model.SesscodeReq;
import com.nortal.sk.ws.model.SesscodeReqImpl;
import com.nortal.sk.ws.model.SignedDocRsp;

import ee.sk.digidoc.SignedDoc;

public class SignedDocStep extends WSStepImpl<SesscodeReq> {
  {
    setCode(StepCodeEnum.SIGNED_DOC);
    setType(SesscodeReqImpl.class);
  }

  @Override
  public GeneralRsp innerExecute() throws Exception {
    SesscodeReq req = prepareRequest();
    SignedDocRsp rsp = getClient().getSignedDoc(req.getSesscode());

    SignedDoc doc = getState().getDoc();
    if (StringUtils.isNotBlank(rsp.getSignedDocData()) && doc != null) {
      getState().setDoc(DigiDocClientUtils.readSignedDoc(DigiDocClientUtils.fromWSFormat(doc, rsp.getSignedDocData())));
    }
    return rsp;
  }
}

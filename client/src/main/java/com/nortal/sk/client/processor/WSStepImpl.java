package com.nortal.sk.client.processor;

import javax.xml.ws.soap.SOAPFaultException;

import org.apache.commons.beanutils.BeanUtils;

import com.nortal.sk.ws.DigiDocServiceClient;
import com.nortal.sk.ws.model.FaultRsp;
import com.nortal.sk.ws.model.GeneralReq;
import com.nortal.sk.ws.model.SesscodeReq;
import com.nortal.sk.ws.model.SesscodeRsp;

public abstract class WSStepImpl<T extends GeneralReq> extends AbstractStep<T> implements WSStep {
  private DigiDocServiceClient client;
  private Class<? extends GeneralReq> type;

  protected DigiDocServiceClient getClient() {
    return client;
  }

  @Override
  public void setClient(DigiDocServiceClient client) {
    this.client = client;
  }

  @Override
  public boolean isComplete() {
    return isValid();
  }

  protected <V extends GeneralReq> void setType(Class<V> type) {
    this.type = type;
  }

  @SuppressWarnings("unchecked")
  protected <V extends GeneralReq> Class<V> getType() {
    return (Class<V>) type;
  }

  @SuppressWarnings("unchecked")
  protected <V extends GeneralReq> V prepareRequest() throws Exception {
    V req = (V) getType().newInstance();
    if (getInput() != null) {
      BeanUtils.copyProperties(req, getInput());
    }

    if (SesscodeReq.class.isAssignableFrom(req.getClass())) {
      SesscodeRsp rsp = getState().getResponse(SesscodeRsp.class);
      if (rsp != null) {
        ((SesscodeReq) req).setSesscode(rsp.getSesscode());
      }
    }
    return req;
  }

  @Override
  public void execute() throws Exception {
    try {
      super.execute();
    }
    catch (SOAPFaultException e) {
      getState().setResponse(getCode(), FaultRsp.of(e.getFault().getFaultString(), e.getFault().getDetail().getTextContent()));
    }
  }
}

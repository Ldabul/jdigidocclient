package com.nortal.sk.client.processor;

/**
 * @author Lauri Lättemäe <lauri.lattemae@nortal.com>
 */
public interface StepDataCallback<T> {
  T getData();
}

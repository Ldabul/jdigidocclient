package com.nortal.sk.client.processor;

import com.nortal.sk.client.constant.StepCodeEnum;
import com.nortal.sk.ws.constant.StatusEnum;
import com.nortal.sk.ws.model.GeneralReq;
import com.nortal.sk.ws.model.GeneralRsp;
import com.nortal.sk.ws.model.StatusRsp;

public abstract class AbstractStep<T extends GeneralReq> implements Step {
  private StateHolder state;
  private StepCodeEnum code;
  @SuppressWarnings("rawtypes")
  private Enum validStatus = StatusEnum.OK;
  private T input;
  private boolean returning;

  protected StateHolder getState() {
    return state;
  }

  @Override
  public void setState(StateHolder state) {
    this.state = state;
  }

  @Override
  public StepCodeEnum getCode() {
    return code;
  }

  protected void setCode(StepCodeEnum code) {
    this.code = code;
  }

  @SuppressWarnings("rawtypes")
  protected void setValidStatus(Enum validStatus) {
    this.validStatus = validStatus;
  }

  public void setInput(T input) {
    this.input = input;
  }

  protected T getInput() {
    return input;
  }

  public boolean isReturning() {
    return returning;
  }

  public void setReturning(boolean returning) {
    this.returning = returning;
  }

  @Override
  @SuppressWarnings({ "unchecked", "static-access" })
  public boolean isValid() {
    try {
      return validStatus.equals(validStatus.valueOf(validStatus.getClass(), ((StatusRsp) getResponse()).getStatus()));
    }
    catch (Exception e) {
    }
    return false;
  }

  @Override
  public boolean isComplete() {
    return isValid();
  }

  @Override
  public void execute() throws Exception {
    getState().setResponse(getCode(), innerExecute());
  }

  protected abstract GeneralRsp innerExecute() throws Exception;

  protected <R extends GeneralRsp> R getResponse() {
    return getState().getResponse(getCode());
  }
}

package com.nortal.sk.client.processor;

import com.nortal.sk.client.constant.StepCodeEnum;
import com.nortal.sk.ws.constant.StatusEnum;
import com.nortal.sk.ws.model.GeneralReq;
import com.nortal.sk.ws.model.GeneralRsp;
import com.nortal.sk.ws.model.StatusRspImpl;

import ee.sk.digidoc.Signature;
import ee.sk.digidoc.SignedDoc;

public class SignatureConfirmationStep extends AbstractStep<GeneralReq> {
  {
    setCode(StepCodeEnum.SIGNATURE_CONFIRMATION);
  }

  @Override
  protected GeneralRsp innerExecute() throws Exception {
    SignedDoc doc = getState().getDoc();
    Signature sig = doc.getSignature(doc.getSignatures().size() - 1);
    sig.getConfirmation();
    return StatusRspImpl.of(StatusEnum.OK);
  }
}

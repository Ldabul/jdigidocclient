package com.nortal.sk.client.model;

import java.security.cert.X509Certificate;

import com.nortal.sk.ws.model.GeneralReq;

import ee.sk.digidoc.SignatureProductionPlace;

public interface CardSignStartReq extends GeneralReq {
  X509Certificate getCert();

  String[] getClaimedRoles();

  SignatureProductionPlace getAdr();
}

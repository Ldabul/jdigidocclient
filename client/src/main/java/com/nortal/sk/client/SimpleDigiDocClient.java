package com.nortal.sk.client;

import javax.annotation.PostConstruct;

import ee.sk.utils.ConfigManager;

/**
 * @author Lauri Lättemäe <lauri.lattemae@nortal.com>
 */
public class SimpleDigiDocClient extends AbstractDigiDocClient {

  private String configLocation = "jar://jdigidoc.cfg";

  @PostConstruct
  private void init() {
    System.out.println("SimpleDigiDocClient.init: " + configLocation);
    if (ConfigManager.init(configLocation)) {
      ConfigManager.addProvider();
    }
  }

  public void setConfigLocation(String configLocation) {
    System.out.println("SET CONFIG: " + configLocation);
    this.configLocation = configLocation;
  }
}

package com.nortal.sk.client.processor;

import com.nortal.sk.client.constant.StepCodeEnum;
import com.nortal.sk.ws.constant.StatusEnum;
import com.nortal.sk.ws.model.GeneralRsp;
import com.nortal.sk.ws.model.MobileSignReq;
import com.nortal.sk.ws.model.MobileSignReqImpl;

public class MobileSignStep extends WSStepImpl<MobileSignReq> {
  {
    setCode(StepCodeEnum.MOBILE_SIGN);
    setValidStatus(StatusEnum.OK);
    setType(MobileSignReqImpl.class);
    setReturning(true);
  }

  @Override
  protected GeneralRsp innerExecute() throws Exception {
    return getClient().mobileSign((MobileSignReq) prepareRequest());
  }

  public static MobileSignStep of(MobileSignReq req) {
    MobileSignStep step = new MobileSignStep();
    step.setInput(req);
    return step;
  }
}

package com.nortal.sk.client.processor;

import com.nortal.sk.client.constant.StepCodeEnum;
import com.nortal.sk.ws.constant.StatusEnum;
import com.nortal.sk.ws.model.GeneralRsp;
import com.nortal.sk.ws.model.SesscodeReq;
import com.nortal.sk.ws.model.SesscodeReqImpl;

/**
 * @author Lauri Lättemäe <lauri.lattemae@nortal.com>
 */
@Deprecated
public class CloseSessionStep extends WSStepImpl<SesscodeReq> {
  {
    setCode(StepCodeEnum.CLOSE_SESSION);
    setValidStatus(StatusEnum.OK);
    setType(SesscodeReqImpl.class);
  }

  @Override
  protected GeneralRsp innerExecute() throws Exception {
    SesscodeReq req = prepareRequest();
    return getClient().closeSession(req.getSesscode());
  }
}

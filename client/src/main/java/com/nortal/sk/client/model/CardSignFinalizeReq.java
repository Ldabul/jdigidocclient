package com.nortal.sk.client.model;

import com.nortal.sk.ws.model.GeneralReq;

public interface CardSignFinalizeReq extends GeneralReq {
  String getSignatureHex();
}

package com.nortal.sk.client.processor;

import com.nortal.sk.client.constant.StepCodeEnum;
import com.nortal.sk.client.model.CardSignStartReq;
import com.nortal.sk.client.model.CardSignStartRsp;
import com.nortal.sk.ws.model.GeneralRsp;

import ee.sk.digidoc.Signature;
import ee.sk.digidoc.SignedDoc;

public class CardSignStartStep extends AbstractStep<CardSignStartReq> {
  {
    setCode(StepCodeEnum.CARD_SIGN_START);
    setReturning(true);
  }

  @Override
  protected GeneralRsp innerExecute() throws Exception {
    SignedDoc doc = getState().getDoc();
    CardSignStartReq req = getInput();
    Signature sig = doc.prepareSignature(req.getCert(), req.getClaimedRoles(), req.getAdr());
    SignedDoc.bin2hex(sig.calculateSignedInfoDigest());
    return CardSignStartRsp.of(SignedDoc.bin2hex(sig.calculateSignedInfoDigest()));
  }

  public static CardSignStartStep of(CardSignStartReq req) {
    CardSignStartStep step = new CardSignStartStep();
    step.setInput(req);
    return step;
  }
}

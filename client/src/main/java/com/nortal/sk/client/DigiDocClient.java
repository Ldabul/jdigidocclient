package com.nortal.sk.client;

import com.nortal.sk.client.processor.Processor;
import com.nortal.sk.client.processor.StateHolder;

/**
 * @author Lauri Lättemäe <lauri.lattemae@nortal.com>
 */
public interface DigiDocClient {
  Processor createProcessor();

  Processor createProcessor(StateHolder state);
}

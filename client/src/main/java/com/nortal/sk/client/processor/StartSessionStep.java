package com.nortal.sk.client.processor;

import org.apache.commons.lang3.StringUtils;

import com.nortal.sk.client.DigiDocClientUtils;
import com.nortal.sk.client.constant.StepCodeEnum;
import com.nortal.sk.ws.constant.StatusEnum;
import com.nortal.sk.ws.model.GeneralRsp;
import com.nortal.sk.ws.model.StartSessionReq;
import com.nortal.sk.ws.model.StartSessionReqImpl;

/**
 * @author Lauri Lättemäe <lauri.lattemae@nortal.com>
 */
public class StartSessionStep extends WSStepImpl<StartSessionReq> {
  {
    setCode(StepCodeEnum.START_SESSION);
    setValidStatus(StatusEnum.OK);
    setType(StartSessionReqImpl.class);
  }

  @Override
  protected GeneralRsp innerExecute() throws Exception {
    StartSessionReqImpl req = prepareRequest();
    if (StringUtils.isEmpty(req.getSigDocXML()) && getState().getDoc() != null) {
      req.setSigDocXML(DigiDocClientUtils.toWSFormat(getState().getDoc()));
    }
    return getClient().startSession(req);
  }

  public static StartSessionStep of(StartSessionReq req) throws Exception {
    StartSessionStep step = new StartSessionStep();
    step.setInput(req);
    return step;
  }
}

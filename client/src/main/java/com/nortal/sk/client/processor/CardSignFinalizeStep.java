package com.nortal.sk.client.processor;

import com.nortal.sk.client.constant.StepCodeEnum;
import com.nortal.sk.client.model.CardSignFinalizeReq;
import com.nortal.sk.ws.constant.StatusEnum;
import com.nortal.sk.ws.model.GeneralRsp;
import com.nortal.sk.ws.model.StatusRspImpl;

import ee.sk.digidoc.Signature;
import ee.sk.digidoc.SignedDoc;

public class CardSignFinalizeStep extends AbstractStep<CardSignFinalizeReq> {
  {
    setCode(StepCodeEnum.CARD_SIGN_FINALIZE);
  }

  @Override
  protected GeneralRsp innerExecute() throws Exception {
    SignedDoc doc = getState().getDoc();
    CardSignFinalizeReq req = getInput();
    Signature sig = doc.getSignature(doc.getSignatures().size() - 1);
    sig.setSignatureValue(SignedDoc.hex2bin(req.getSignatureHex()));
    return StatusRspImpl.of(StatusEnum.OK);
  }

  public static CardSignFinalizeStep of(CardSignFinalizeReq req) {
    CardSignFinalizeStep step = new CardSignFinalizeStep();
    step.setInput(req);
    return step;
  }
}

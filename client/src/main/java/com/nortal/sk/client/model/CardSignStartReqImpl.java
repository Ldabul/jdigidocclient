package com.nortal.sk.client.model;

import java.security.cert.X509Certificate;

import ee.sk.digidoc.DigiDocException;
import ee.sk.digidoc.SignatureProductionPlace;
import ee.sk.digidoc.SignedDoc;

public class CardSignStartReqImpl implements CardSignStartReq {
  private X509Certificate cert;
  private String[] claimedRoles;
  private SignatureProductionPlace adr;

  @Override
  public X509Certificate getCert() {
    return cert;
  }

  public void setCert(X509Certificate cert) {
    this.cert = cert;
  }

  @Override
  public String[] getClaimedRoles() {
    return claimedRoles;
  }

  public void setClaimedRoles(String[] claimedRoles) {
    this.claimedRoles = claimedRoles;
  }

  @Override
  public SignatureProductionPlace getAdr() {
    return adr;
  }

  public void setAdr(SignatureProductionPlace adr) {
    this.adr = adr;
  }

  public static CardSignStartReqImpl of(String certHex) throws DigiDocException {
    CardSignStartReqImpl req = new CardSignStartReqImpl();
    req.setCert(SignedDoc.readCertificate(SignedDoc.hex2bin(certHex)));
    return req;
  }
}

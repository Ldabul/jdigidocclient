package com.nortal.sk.client.test;

import java.io.File;
import java.io.FileOutputStream;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.nortal.sk.client.DigiDocClient;
import com.nortal.sk.client.DigiDocClientUtils;
import com.nortal.sk.client.processor.MobileSignStep;
import com.nortal.sk.client.processor.Processor;
import com.nortal.sk.client.processor.SignedDocStep;
import com.nortal.sk.client.processor.StartSessionStep;
import com.nortal.sk.client.processor.StateHolder;
import com.nortal.sk.client.processor.StatusInfoStep;
import com.nortal.sk.ws.model.ChallengeRsp;
import com.nortal.sk.ws.model.FaultRsp;
import com.nortal.sk.ws.model.MobileSignReqImpl;

public class ddocTest {

  public static void main(String[] args) {

    try {
      System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");

      ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
      DigiDocClient client = context.getBean(DigiDocClient.class);

      //client.init();
      StateHolder state = StateHolder.of(DigiDocClientUtils.readSignedDoc(new File("C:/projects/jDigiDocClient/client/1336810.ddoc")));
      do {
        // @formatter:off
        Processor processor = client.createProcessor(state)
            .step(new StartSessionStep())
            .step(MobileSignStep.of(MobileSignReqImpl.of("14212128025", "37200007")))
            .step(new StatusInfoStep())
            .step(new SignedDocStep());
        // @formatter:on
        state = processor.process();

        if (state.isValid()) {
          switch (state.getActiveStep()) {
            case MOBILE_SIGN:
              ChallengeRsp rsp = state.getActiveResponse();
              System.out.println("CHALLENGE: " + rsp.getChallengeID());
              break;
            case STATUS_INFO:
              Thread.currentThread().sleep(2000);
            default:
              System.out.println(state.getActiveStep());
              break;
          }
        }
      } while (!state.isComplete());

      if (state.isValid()) {
        state.getDoc().writeToStream(new FileOutputStream("C:/projects/jDigiDocClient/client/out.ddoc"));
      }
      else {
        FaultRsp rsp = state.getActiveResponse();
        System.out.println("code=" + rsp.getCode() + ", message=" + rsp.getMessage());
      }
    }
    catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

}

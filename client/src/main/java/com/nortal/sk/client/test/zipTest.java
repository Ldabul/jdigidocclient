package com.nortal.sk.client.test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang3.StringUtils;

import com.nortal.sk.client.DigiDocClientUtils;
import com.nortal.sk.client.model.FileEntryType;
import com.nortal.sk.client.model.HashcodesType;
import com.nortal.sk.client.model.ObjectFactory;

import ee.sk.digidoc.Base64Util;
import ee.sk.digidoc.DataFile;
import ee.sk.digidoc.DataFileAttribute;
import ee.sk.digidoc.DigiDocException;
import ee.sk.digidoc.SignedDoc;
import ee.sk.utils.ConfigManager;

public class zipTest {

  public static void main(String[] args) {
    try {
      ConfigManager.init("jar://jdigidoc.cfg");
      ConfigManager.addProvider();

      System.out.println(DigiDocClientUtils.isBDOC(new File("C:/projects/jDigiDocClient/client/test.ddoc")));
      SignedDoc doc = DigiDocClientUtils.readSignedDoc(new File("C:/projects/jDigiDocClient/client/test.ddoc"));
      //System.out.println(doc.toXML());

      List<DataFile> dataFiles = doc.getDataFiles();
      for (DataFile df : dataFiles) {
        df.setContentType(DataFile.CONTENT_HASHCODE);
        // df.setDigest(df.getDigestValueOfType(DataFile.DIGEST_TYPE_SHA1));
        df.addAttribute(new DataFileAttribute("DigestType", DataFile.DIGEST_TYPE_SHA1));
        df.addAttribute(new DataFileAttribute("DigestValue", Base64Util.encode(df.getDigestValueOfType(DataFile.DIGEST_TYPE_SHA1), 0)));
        //df.setBody(new byte[0]);
      }
      System.out.println(doc.toXML());
      //
      //      byte[] s1doc = prepareDoc(doc);
      //      FileOutputStream fos = new FileOutputStream("C:/projects/jDigiDocClient/client/test_s1.bdoc");
      //      fos.write(s1doc);
      //      fos.close();
      //
      //      byte[] s2doc = finalizeDoc(doc, s1doc);
      //      fos = new FileOutputStream("C:/projects/jDigiDocClient/client/test_s2.bdoc");
      //      fos.write(s2doc);
      //      fos.close();
    }
    catch (DigiDocException e) {
      System.out.println(e.getCode() + " " + e.getMessage());
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  @SuppressWarnings("unchecked")
  private static byte[] finalizeDoc(SignedDoc doc, byte[] content) throws IOException, DigiDocException {
    ByteArrayOutputStream os = new ByteArrayOutputStream();
    ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(content)); // FileInputStream("C:/projects/jDigiDocClient/client/test.bdoc"));
    ZipOutputStream zos = new ZipOutputStream(os);
    //      zis.ge
    //      ZipFile zip = new ZipFile(new File("C:/projects/jDigiDocClient/client/test.bdoc"));
    //      Enumeration<? extends ZipEntry> entries = zis.getNextEntry().entries();

    int len;
    byte[] buf = new byte[1024];
    ZipEntry entry = null;
    while ((entry = zis.getNextEntry()) != null) {
      if (!StringUtils.startsWithIgnoreCase(entry.getName(), "META-INF/hashcodes-sha")) {
        zos.putNextEntry(new ZipEntry(entry));
        while ((len = zis.read(buf)) > 0) {
          zos.write(buf, 0, len);
        }
      }
    }

    List<DataFile> dataFiles = doc.getDataFiles();
    for (DataFile df : dataFiles) {
      zos.putNextEntry(new ZipEntry(df.getFileName()));
      zos.write(df.getBody());
    }
    zis.close();
    zos.close();

    return os.toByteArray();
  }

  private static byte[] prepareDoc(SignedDoc doc) throws IOException, DigiDocException, JAXBException {
    ByteArrayOutputStream os = new ByteArrayOutputStream();
    ByteArrayOutputStream dos = new ByteArrayOutputStream();
    doc.writeToStream(dos);

    ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(dos.toByteArray())); // FileInputStream("C:/projects/jDigiDocClient/client/test.bdoc"));
    ZipOutputStream zos = new ZipOutputStream(os);
    //      zis.ge
    //      ZipFile zip = new ZipFile(new File("C:/projects/jDigiDocClient/client/test.bdoc"));
    //      Enumeration<? extends ZipEntry> entries = zis.getNextEntry().entries();

    int len;
    byte[] buf = new byte[1024];
    ZipEntry entry = null;
    while ((entry = zis.getNextEntry()) != null) {
      if (StringUtils.equalsIgnoreCase(entry.getName(), "mimetype") || StringUtils.startsWithIgnoreCase(entry.getName(), "META-INF/")) {
        zos.putNextEntry(new ZipEntry(entry));
        while ((len = zis.read(buf)) > 0) {
          zos.write(buf, 0, len);
        }
      }
    }
    zos.putNextEntry(new ZipEntry("META-INF/hashcodes-sha256.xml"));
    zos.write(toHashCode(doc, SignedDoc.SHA256_DIGEST_TYPE));
    zos.putNextEntry(new ZipEntry("META-INF/hashcodes-sha512.xml"));
    zos.write(toHashCode(doc, SignedDoc.SHA512_DIGEST_TYPE));

    zis.close();
    zos.close();
    return os.toByteArray();
  }

  @SuppressWarnings("unchecked")
  private static byte[] toHashCode(SignedDoc doc, String digestType) throws DigiDocException, JAXBException {
    List<DataFile> dataFiles = doc.getDataFiles();
    ObjectFactory of = new ObjectFactory();
    HashcodesType hashcodes = of.createHashcodesType();
    for (DataFile df : dataFiles) {
      FileEntryType entry = of.createFileEntryType();
      entry.setFullPath(df.getFileName());
      entry.setSize(df.getSize());
      entry.setHash(Base64Util.encode(df.getDigestValueOfType(digestType)));
      hashcodes.getFileEntry().add(entry);
    }

    JAXBContext context = JAXBContext.newInstance("com.nortal.sk.client.model");
    Marshaller m = context.createMarshaller();
    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
    StringWriter sw = new StringWriter();
    m.marshal(of.createHashcodes(hashcodes), sw);

    return sw.toString().getBytes();
  }
}

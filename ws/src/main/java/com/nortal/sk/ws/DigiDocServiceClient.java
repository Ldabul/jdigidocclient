package com.nortal.sk.ws;

import com.nortal.sk.ws.model.DataFileData;
import com.nortal.sk.ws.model.MobileAuthenticateReq;
import com.nortal.sk.ws.model.MobileAuthenticateRsp;
import com.nortal.sk.ws.model.MobileAuthenticateStatusReq;
import com.nortal.sk.ws.model.MobileAuthenticateStatusRsp;
import com.nortal.sk.ws.model.MobileSignReq;
import com.nortal.sk.ws.model.MobileSignRsp;
import com.nortal.sk.ws.model.SignedDocRsp;
import com.nortal.sk.ws.model.StartSessionReq;
import com.nortal.sk.ws.model.StartSessionRsp;
import com.nortal.sk.ws.model.StatusInfoReq;
import com.nortal.sk.ws.model.StatusInfoRsp;
import com.nortal.sk.ws.model.StatusRsp;

/**
 * @author Lauri Lättemäe <lauri.lattemae@nortal.com>
 */
public interface DigiDocServiceClient {

  // Signing methods
  StartSessionRsp startSession(String signingProfile, String sigDocXML, boolean bHoldSession, DataFileData datafile);

  StartSessionRsp startSession(StartSessionReq req);

  MobileSignRsp mobileSign(int sesscode,
                           String signerIDCode,
                           String signersCountry,
                           String signerPhoneNo,
                           String serviceName,
                           String additionalDataToBeDisplayed,
                           String language,
                           String role,
                           String city,
                           String stateOrProvince,
                           String postalCode,
                           String countryName,
                           String signingProfile,
                           String messagingMode,
                           int asyncConfiguration,
                           boolean returnDocInfo,
                           boolean returnDocData);

  MobileSignRsp mobileSign(MobileSignReq req);

  /**
   * 8.6 GetStatusInfo
   * 
   * @param sesscode
   * @param returnDocInfo
   * @param waitSignature
   * @return
   */
  StatusInfoRsp getStatusInfo(int sesscode, boolean returnDocInfo, boolean waitSignature);

  /**
   * 8.6 GetStatusInfo
   * 
   * @param req
   * @return
   */
  StatusInfoRsp getStatusInfo(StatusInfoReq req);

  /**
   * 8.2 CloseSession
   * 
   * @param sesscode
   * @return
   */
  StatusRsp closeSession(int sesscode);

  /**
   * 8.8 GetSignedDoc
   * 
   * @param sesscode
   * @return
   */
  SignedDocRsp getSignedDoc(int sesscode);

  // Mobile auth methods
  MobileAuthenticateRsp mobileAuthenticate(String idCode,
                                           String countryCode,
                                           String phoneNo,
                                           String language,
                                           String serviceName,
                                           String messageToDisplay,
                                           String spChallenge,
                                           String messagingMode,
                                           int asyncConfiguration,
                                           boolean returnCertData,
                                           boolean returnRevocationData);

  MobileAuthenticateRsp mobileAuthenticate(MobileAuthenticateReq req);

  MobileAuthenticateStatusRsp getMobileAuthenticateStatus(int sesscode, boolean waitSignature);

  MobileAuthenticateStatusRsp getMobileAuthenticateStatus(MobileAuthenticateStatusReq req);
}
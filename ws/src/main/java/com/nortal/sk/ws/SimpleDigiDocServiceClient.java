package com.nortal.sk.ws;

import javax.annotation.PostConstruct;

import com.nortal.sk.ws.model.DigiDocService;

public class SimpleDigiDocServiceClient extends AbstractDigiDocServiceClient {
  {
    setDigiDocService(new DigiDocService(getClass().getResource("/DigiDocService_2_3.wsdl")));
    setEndpoint("https://www.openxades.org:9443/DigiDocService");
    setServiceName("Testimine");
  }

  @PostConstruct
  private void init() {
    System.out.println("DIGIDOCSERVICE: " + getEnpoint() + " " + getServiceName());
  }
}

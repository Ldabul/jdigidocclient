package com.nortal.sk.ws.model;

public class MobileAuthenticateStatusRsp extends StatusRspImpl {
  private String signature;

  public String getSignature() {
    return signature;
  }

  public void setSignature(String signature) {
    this.signature = signature;
  }
}

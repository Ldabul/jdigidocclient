package com.nortal.sk.ws.constant;

/**
 * @author Lauri Lättemäe <lauri.lattemae@nortal.com>
 */
public interface GeneralConstant {
  String getCode();
}

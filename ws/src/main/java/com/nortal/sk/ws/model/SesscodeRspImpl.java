package com.nortal.sk.ws.model;

public class SesscodeRspImpl extends StatusRspImpl implements SesscodeRsp {
  private Integer sesscode;

  @Override
  public Integer getSesscode() {
    return sesscode;
  }

  public void setSesscode(Integer sesscode) {
    this.sesscode = sesscode;
  }
}

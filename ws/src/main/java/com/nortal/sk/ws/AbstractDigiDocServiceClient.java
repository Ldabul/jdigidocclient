package com.nortal.sk.ws;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import org.apache.commons.lang3.StringUtils;

import com.nortal.sk.ws.model.DataFileData;
import com.nortal.sk.ws.model.DigiDocService;
import com.nortal.sk.ws.model.DigiDocServicePortType;
import com.nortal.sk.ws.model.MobileAuthenticateReq;
import com.nortal.sk.ws.model.MobileAuthenticateRsp;
import com.nortal.sk.ws.model.MobileAuthenticateStatusReq;
import com.nortal.sk.ws.model.MobileAuthenticateStatusRsp;
import com.nortal.sk.ws.model.MobileSignReq;
import com.nortal.sk.ws.model.MobileSignRsp;
import com.nortal.sk.ws.model.SignedDocInfo;
import com.nortal.sk.ws.model.SignedDocRsp;
import com.nortal.sk.ws.model.StartSessionReq;
import com.nortal.sk.ws.model.StartSessionRsp;
import com.nortal.sk.ws.model.StatusInfoReq;
import com.nortal.sk.ws.model.StatusInfoRsp;
import com.nortal.sk.ws.model.StatusRsp;
import com.nortal.sk.ws.model.StatusRspImpl;

/**
 * @author Lauri Lättemäe <lauri.lattemae@nortal.com>
 */
public class AbstractDigiDocServiceClient implements DigiDocServiceClient {
  private DigiDocService digiDocService;
  private String endpoint;
  private String serviceName;

  public void setDigiDocService(DigiDocService digiDocService) {
    this.digiDocService = digiDocService;
  }

  public String getEnpoint() {
    return endpoint;
  }

  public void setEndpoint(String endpoint) {
    this.endpoint = endpoint;
  }

  public String getServiceName() {
    return serviceName;
  }

  public void setServiceName(String serviceName) {
    this.serviceName = serviceName;
  }

  private DigiDocServicePortType getPort() {
    DigiDocServicePortType port = digiDocService.getDigiDocService();
    if (StringUtils.isNotBlank(getEnpoint())) {
      ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, getEnpoint());
    }
    return port;
  }

  protected <T> T nvl(T v1, T v2) {
    if (v1 != null && (!String.class.isAssignableFrom(v1.getClass()) || (StringUtils.isNotBlank((String) v1)))) {
      return v1;
    }
    return v2;
  }

  // SIGNING METHODS
  @Override
  public StartSessionRsp startSession(String signingProfile, String sigDocXML, boolean bHoldSession, DataFileData datafile) {
    Holder<String> status = new Holder<>();
    Holder<Integer> sesscode = new Holder<>();
    Holder<SignedDocInfo> signedDocInfo = new Holder<>();

    getPort().startSession(signingProfile, sigDocXML, bHoldSession, datafile, status, sesscode, signedDocInfo);

    StartSessionRsp rsp = new StartSessionRsp();
    rsp.setStatus(status.value);
    rsp.setSesscode(sesscode.value);
    rsp.setSignedDocInfo(signedDocInfo.value);
    return rsp;
  }

  @Override
  public StartSessionRsp startSession(StartSessionReq req) {
    return startSession(req.getSigningProfile(), req.getSigDocXML(), req.isbHoldSession(), req.getDataFile());
  }

  @Override
  public MobileSignRsp mobileSign(int sesscode,
                                  String signerIDCode,
                                  String signersCountry,
                                  String signerPhoneNo,
                                  String serviceName,
                                  String additionalDataToBeDisplayed,
                                  String language,
                                  String role,
                                  String city,
                                  String stateOrProvince,
                                  String postalCode,
                                  String countryName,
                                  String signingProfile,
                                  String messagingMode,
                                  int asyncConfiguration,
                                  boolean returnDocInfo,
                                  boolean returnDocData) {
    Holder<String> status = new Holder<>();
    Holder<String> statusCode = new Holder<>();
    Holder<String> challengeID = new Holder<>();

    getPort().mobileSign(sesscode,
                         signerIDCode,
                         signersCountry,
                         signerPhoneNo,
                         serviceName,
                         additionalDataToBeDisplayed,
                         language,
                         role,
                         city,
                         stateOrProvince,
                         postalCode,
                         countryName,
                         signingProfile,
                         messagingMode,
                         asyncConfiguration,
                         returnDocInfo,
                         returnDocData,
                         status,
                         statusCode,
                         challengeID);

    MobileSignRsp rsp = new MobileSignRsp();
    rsp.setStatus(status.value);
    rsp.setStatusCode(statusCode.value);
    rsp.setChallengeID(challengeID.value);
    return rsp;
  }

  @Override
  public MobileSignRsp mobileSign(MobileSignReq req) {
    return mobileSign(req.getSesscode(),
                      req.getSignerIDCode(),
                      req.getSignersCountry(),
                      req.getSignerPhoneNo(),
                      nvl(req.getServiceName(), getServiceName()),
                      req.getAdditionalDataToBeDisplayed(),
                      req.getLanguage(),
                      req.getRole(),
                      req.getCity(),
                      req.getStateOrProvince(),
                      req.getPostalCode(),
                      req.getCountryName(),
                      req.getSigningProfile(),
                      req.getMessagingMode(),
                      req.getAsyncConfiguration(),
                      req.isReturnDocInfo(),
                      req.isReturnDocData());
  }

  @Override
  public StatusInfoRsp getStatusInfo(int sesscode, boolean returnDocInfo, boolean waitSignature) {
    Holder<String> status = new Holder<>();
    Holder<String> statusCode = new Holder<>();
    Holder<SignedDocInfo> signedDocInfo = new Holder<>();

    getPort().getStatusInfo(sesscode, returnDocInfo, waitSignature, status, statusCode, signedDocInfo);

    StatusInfoRsp rsp = new StatusInfoRsp();
    rsp.setStatus(status.value);
    rsp.setStatusCode(statusCode.value);
    rsp.setSignedDocInfo(signedDocInfo.value);
    return rsp;
  }

  @Override
  public StatusInfoRsp getStatusInfo(StatusInfoReq req) {
    return getStatusInfo(req.getSesscode(), req.isReturnDocInfo(), req.isWaitSignature());
  }

  @Override
  public SignedDocRsp getSignedDoc(int sesscode) {
    Holder<String> status = new Holder<>();
    Holder<String> signedDocData = new Holder<>();

    getPort().getSignedDoc(sesscode, status, signedDocData);

    SignedDocRsp rsp = new SignedDocRsp();
    rsp.setStatus(status.value);
    rsp.setSignedDocData(signedDocData.value);
    return rsp;
  }

  @Override
  public StatusRsp closeSession(int sesscode) {
    return StatusRspImpl.of(getPort().closeSession(sesscode));
  }

  // MOBILE AUTH METHODS
  @Override
  public MobileAuthenticateRsp mobileAuthenticate(String idCode,
                                                  String countryCode,
                                                  String phoneNo,
                                                  String language,
                                                  String serviceName,
                                                  String messageToDisplay,
                                                  String spChallenge,
                                                  String messagingMode,
                                                  int asyncConfiguration,
                                                  boolean returnCertData,
                                                  boolean returnRevocationData) {
    Holder<Integer> sesscode = new Holder<>();
    Holder<String> status = new Holder<>();
    Holder<String> userIDCode = new Holder<>();
    Holder<String> userGivenname = new Holder<>();
    Holder<String> userSurname = new Holder<>();
    Holder<String> userCountry = new Holder<>();
    Holder<String> userCN = new Holder<>();
    Holder<String> certificateData = new Holder<>();
    Holder<String> challengeID = new Holder<>();
    Holder<String> challenge = new Holder<>();
    Holder<String> revocationData = new Holder<>();

    getPort().mobileAuthenticate(idCode,
                                 countryCode,
                                 phoneNo,
                                 language,
                                 serviceName,
                                 messageToDisplay,
                                 spChallenge,
                                 messagingMode,
                                 asyncConfiguration,
                                 returnCertData,
                                 returnRevocationData,
                                 sesscode,
                                 status,
                                 userIDCode,
                                 userGivenname,
                                 userSurname,
                                 userCountry,
                                 userCN,
                                 certificateData,
                                 challengeID,
                                 challenge,
                                 revocationData);

    MobileAuthenticateRsp rsp = new MobileAuthenticateRsp();
    rsp.setSesscode(sesscode.value);
    rsp.setStatus(status.value);
    rsp.setUserIDCode(userIDCode.value);
    rsp.setUserGivenname(userGivenname.value);
    rsp.setUserCountry(userCountry.value);
    rsp.setUserCN(userCN.value);
    rsp.setCertificateData(certificateData.value);
    rsp.setChallengeID(challengeID.value);
    rsp.setChallenge(challenge.value);
    rsp.setRevocationData(revocationData.value);

    return rsp;
  }

  @Override
  public MobileAuthenticateRsp mobileAuthenticate(MobileAuthenticateReq req) {
    return mobileAuthenticate(req.getIdCode(),
                              req.getCountryCode(),
                              req.getPhoneNo(),
                              req.getLanguage(),
                              nvl(req.getServiceName(), getServiceName()),
                              req.getMessageToDisplay(),
                              req.getSpChallenge(),
                              req.getMessagingMode(),
                              req.getAsyncConfiguration(),
                              req.isReturnCertData(),
                              req.isReturnRevocationData());
  }

  @Override
  public MobileAuthenticateStatusRsp getMobileAuthenticateStatus(int sesscode, boolean waitSignature) {
    Holder<String> status = new Holder<>();
    Holder<String> signature = new Holder<>();

    getPort().getMobileAuthenticateStatus(sesscode, waitSignature, status, signature);

    MobileAuthenticateStatusRsp rsp = new MobileAuthenticateStatusRsp();
    rsp.setStatus(status.value);
    rsp.setSignature(signature.value);
    return rsp;
  }

  @Override
  public MobileAuthenticateStatusRsp getMobileAuthenticateStatus(MobileAuthenticateStatusReq req) {
    return getMobileAuthenticateStatus(req.getSesscode(), req.isWaitSignature());
  }
}

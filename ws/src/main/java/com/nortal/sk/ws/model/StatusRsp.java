package com.nortal.sk.ws.model;

/**
 * @author Lauri Lättemäe <lauri.lattemae@nortal.com>
 */
public interface StatusRsp extends GeneralRsp {
  String getStatus();
}

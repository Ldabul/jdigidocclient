package com.nortal.sk.ws.constant;

/**
 * @author Lauri Lättemäe <lauri.lattemae@nortal.com>
 */
public enum StatusEnum implements GeneralConstant {
  OK;

  @Override
  public String getCode() {
    return name();
  }
}

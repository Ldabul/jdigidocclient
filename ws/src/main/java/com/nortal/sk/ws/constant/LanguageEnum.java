package com.nortal.sk.ws.constant;

/**
 * @author Lauri Lättemäe <lauri.lattemae@nortal.com>
 */
public enum LanguageEnum implements GeneralConstant {
  EST,
  ENG,
  RUS,
  LIT;

  @Override
  public String getCode() {
    return name();
  }
}

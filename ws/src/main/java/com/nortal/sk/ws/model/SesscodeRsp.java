package com.nortal.sk.ws.model;

public interface SesscodeRsp extends StatusRsp {
  /**
   * Session code used for further requests in the given transaction
   * 
   * @return
   */
  Integer getSesscode();
}

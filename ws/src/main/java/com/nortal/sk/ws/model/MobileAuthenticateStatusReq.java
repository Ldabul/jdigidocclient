package com.nortal.sk.ws.model;

public interface MobileAuthenticateStatusReq extends SesscodeReq {
  boolean isWaitSignature();
}

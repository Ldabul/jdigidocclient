package com.nortal.sk.ws.constant;

/**
 * @author Lauri Lättemäe <lauri.lattemae@nortal.com>
 */
public enum CountryEnum implements GeneralConstant {
  EE;

  @Override
  public String getCode() {
    return name();
  }
}

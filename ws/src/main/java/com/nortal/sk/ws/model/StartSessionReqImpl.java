package com.nortal.sk.ws.model;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Lauri Lättemäe <lauri.lattemae@nortal.com>
 */
public class StartSessionReqImpl implements StartSessionReq {
  private String signingProfile = StringUtils.EMPTY;
  private String sigDocXML = StringUtils.EMPTY;
  private boolean bHoldSession = true;
  private DataFileData dataFile = new DataFileData();

  public String getSigningProfile() {
    return signingProfile;
  }

  public void setSigningProfile(String signingProfile) {
    this.signingProfile = signingProfile;
  }

  public String getSigDocXML() {
    return sigDocXML;
  }

  public void setSigDocXML(String sigDocXML) {
    this.sigDocXML = sigDocXML;
  }

  public boolean isbHoldSession() {
    return bHoldSession;
  }

  public void setbHoldSession(boolean bHoldSession) {
    this.bHoldSession = bHoldSession;
  }

  public DataFileData getDataFile() {
    return dataFile;
  }

  public void setDatafile(DataFileData dataFile) {
    this.dataFile = dataFile;
  }

  public static StartSessionReqImpl of(String sigDocXML) {
    StartSessionReqImpl req = new StartSessionReqImpl();
    req.setSigDocXML(sigDocXML);
    return req;
  }
}
